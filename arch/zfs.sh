#!/usr/bin/env sh

VERSION="0.0.1"

cmd_usage() {
    echo "TODO"
}

cmd_version() {
    echo $VERSION
}

# Add zfs module to the running archiso system
# https://github.com/eoli3n/archiso-zfs
cmd_init_zfs() {
    curl -s https://eoli3n.github.io/archzfs/init | bash
    modprobe zfs
}

# Partition drive
# https://wiki.archlinux.org/title/Install_Arch_Linux_on_ZFS#Partition_the_destination_drive
cmd_format_drive() {
    parted --script /dev/sdx mklabel gpt mkpart non-fs 0% 2 mkpart primary 2 100% set 1 bios_grub on set 2 boot on
}

cmd_init_zpool() {
    # Create encrypted zpool
    # https://wiki.archlinux.org/title/Install_Arch_Linux_on_ZFS#Compression_and_native_encryption
    zpool create -f -o ashift=12  \
        -O acltype=posixacl       \
        -O relatime=on            \
        -O xattr=sa               \
        -O dnodesize=legacy       \
        -O normalization=formD    \
        -O mountpoint=none        \
        -O canmount=off           \
        -O devices=off            \
        -R /mnt                   \
        -O compression=lz4        \
        -O encryption=aes-256-gcm \
        -O keyformat=passphrase   \
        -O keylocation=prompt     \
        zroot $DEVICE/id-to-partition-partx
    
    # Create datasets
    # https://wiki.archlinux.org/title/Install_Arch_Linux_on_ZFS#System_datasets
    zfs create -o mountpoint=none zroot/data
    zfs create -o mountpoint=none zroot/ROOT
    zfs create -o mountpoint=/ -o canmount=noauto zroot/ROOT/default
    zfs create -o mountpoint=/home zroot/data/home

    # Check zfs pool
    # https://wiki.archlinux.org/title/Install_Arch_Linux_on_ZFS#Export/Import_your_pools
    zpool export zroot
    zpool import -d $DEVICE -R /mnt zroot -N

    # Set zfs zpool flags
    # https://wiki.archlinux.org/title/Install_Arch_Linux_on_ZFS#Configure_the_root_filesystem
    zpool set bootfs=zroot/ROOT/default zroot
    zpool set cachefile=/etc/zfs/zpool.cache zroot
}

cmd_mount_zpool() {
    zfs load-key zroot
    zfs mount zroot/ROOT/default
    zfs mount -a
}

cmd_format_zpool() {
    mkfs.ext4 /dev/root_partition
    mkfs.fat -F 32 /dev/efi_system_partition
}

cmd_copy_zpool_cache() {
    cp /etc/zfs/zpool.cache /mnt/etc/zfs/zpool.cache
}

cmd_init_arch() {
    pacstrap /mnt base linux linux-firmware
}

# Mostly standard arch install
# https://wiki.archlinux.org/title/Installation_guide#Installation
cmd_setup_arch() {

    # Execute follow-up commands inside chroot
    arch-chroot /mnt /bin/bash <<EOF

timedatectl set-ntp true

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

hwclock --systohc

echo "en_US ISO UTF" >> /etc/locale.gen

locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo $HOSTNAME > /etc/hostname

echo "1.1.1.1" > /etc/resolv.conf

EOF

}

# Add plymouth boot screen with encrypted zfs support
# https://wiki.archlinux.org/title/plymouth
cmd_init_plymouth() {
    git clone https://aur.archlinux.org/plymouth-zfs.git
    cd plymouth-zfs
    makepkg -si
}

cmd_init_mkinitcpio() {

    /mnt/etc/mkinitcpio-zfs.conf <<EOF
MODULES=()
BINARIES=(rage age-plugin-yubikey)
FILES=()
HOOKS=(base udev autodetect modconf block keyboard plymouth plymouth-zfs filesystems fsck)
EOF

    arch-chroot /mnt /bin/bash <<EOF
mkinitcpio --config /etc/mkinitcpio-zfs.conf --generate /boot/initramfs-zfs.img
EOF

}

cmd_init_bootloader() {
    echo GRUB_CMDLINE_LINUX="root=ZFS=zroot/ROOT/default" >> /etc/default/grub

    /boot/grub/grub.cfg <<EOF
set timeout=5
set default=0

menuentry "Arch Linux" {
    search -u UUID
    linux /ROOT/default/@/boot/vmlinuz-linux zfs=zroot/ROOT/default rw
    initrd /ROOT/default/@/boot/initramfs-linux.img
}
EOF

}

cmd_init() {
    DEVICE="$1"
    HOSTNAME="$2"

    echo "device:    $DEVICE"
    echo "hostname:  $HOSTNAME"

    # cmd_init_zfs
    # cmd_format_drive
    
    # cmd_init_zpool
    # cmd_mount_zpool
    # cmd_format_zpool

    # cmd_init_arch
    # cmd_copy_zpool_cache
    # cmd_setup_arch

    # cmd_init_plymouth
    # cmd_init_mkinitcpio
}

PROGRAM="${0##*/}"
COMMAND="$1"

case "$1" in
	init) shift;			    cmd_init "$@" ;;
	help|--help) shift;		    cmd_usage "$@" ;;
	version|--version) shift;	cmd_version "$@" ;;
	*)				            cmd_usage "$@" ;;
esac
exit 0