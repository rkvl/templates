#!/bin/sh

##
#
#  git --git-dir=./template/.git/ --work-tree=./d7h1/ checkout \*
#
##



SCRIPTPATH=$(dirname "$(readlink -f "$0")")
#cd $SCRIPTPATH


mkdir -p "./$1"

#git --git-dir="${SCRIPTPATH}/.git/" --work-tree=./$1/ checkout '*'

git \
  --git-dir="${SCRIPTPATH}/.git/" \
  --work-tree=./$1/               \
  checkout                        \
    'start.sh'                    \
    'gen.sh'
